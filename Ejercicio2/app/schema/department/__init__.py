import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from app.models import db_session, Department as DepartmentModel


class Department(SQLAlchemyObjectType):
    class Meta:
        model = DepartmentModel
        interfaces = (relay.Node, )


class DepartmentConnection(relay.Connection):
    class Meta:
        node = Department



class CreateDepartment(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        # department = graphene.Int(required=True)
    department = graphene.Field(Department)
    ok = graphene.Boolean()

    def mutate(self, info, name):
        # depart = DepartmentModel.query.get(department)
        department = DepartmentModel(name=name)
        db_session.add(department)
        db_session.commit()
        ok = True
        return CreateDepartment(department=department, ok=ok)


class EditDepartment(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        name = graphene.String(required=True)
    department = graphene.Field(Department)
    ok = graphene.Boolean()

    def mutate(self, info, id, name):
        depart = DepartmentModel.query.get(id)
        if depart == None:
            # return make_response(jsonify(error="La credencial no es valida."), 404)
             return GraphQLError('El departamento no existe')
            # CreateEmployee(employee=None, ok=False)
        # employee = EmployeeModel.query.get(id)
        depart.name = name
        db_session.add(depart)
        db_session.commit()
        ok = True
        return EditDepartment(department=depart, ok=ok)


class DeleteDepartment(graphene.Mutation):
    #Elimina el dpto x id y tambien los empleados que estan asociados a el.
    class Arguments:
        id = graphene.Int()
    department = graphene.Field(Department)
    ok = graphene.Boolean()

    def mutate(self, info, id):
        department = DepartmentModel.query.get(id)
        db_session.delete(department)
        db_session.commit()
        ok = True
        return DeleteDepartment(department=department, ok=ok)
