import graphene
from graphene import relay
from flask import request, jsonify, make_response
from graphql import GraphQLError
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
# from app.schema import schema
from app.models import db_session, Employee as EmployeeModel, Department as DepartmentModel

class Employee(SQLAlchemyObjectType):
    class Meta:
        model = EmployeeModel
        interfaces = (relay.Node, )


class EmployeeConnection(relay.Connection):
    class Meta:
        node = Employee





# class CreateEmployee(graphene.Mutation):
#     class Arguments:
#         name = graphene.String(required=True)
#         department = graphene.Int(required=True)
#     employee = graphene.Field(Employee)
#     ok = graphene.Boolean()
#
#     def mutate(self, info, name, department):
#         depart = DepartmentModel.query.get(department)
#         if depart == None:
#             return GraphQLError('El departamento no existe')
#         employee = EmployeeModel(name=name, department=depart)
#         db_session.add(employee)
#         db_session.commit()
#         ok = True
#         return CreateEmployee(employee=employee, ok=ok)


class EditEmployee(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        name = graphene.String(required=True)
        department = graphene.Int(required=True)
    employee = graphene.Field(Employee)
    ok = graphene.Boolean()

    def mutate(self, info, id, name, department):
        depart = DepartmentModel.query.get(department)
        if depart == None:
            # return make_response(jsonify(error="La credencial no es valida."), 404)
             return GraphQLError('El departamento no existe')
            # CreateEmployee(employee=None, ok=False)
        employee = EmployeeModel.query.get(id)
        employee.name = name
        employee.department = depart
        db_session.add(employee)
        db_session.commit()
        ok = True
        return EditEmployee(employee=employee, ok=ok)



class DeleteEmployee(graphene.Mutation):
    class Arguments:
        id = graphene.Int()
    employee = graphene.Field(Employee)
    ok = graphene.Boolean()

    def mutate(self, info, id):
        employee = EmployeeModel.query.get(id)
        db_session.delete(employee)
        db_session.commit()
        ok = True
        return DeleteEmployee(employee=employee, ok=ok)
