from app import app
from flask import request, jsonify
from app.models import Department, Employee

@app.route('/employee', methods=['POST'])
def CreateEmployee():
    name = request.json['name']
    department = Department.query.get(request.json['department_id'])

    if department == None:
        return jsonify(error="El departamento no existe."), 404
    empleado = Employee(name=name, department=department)
    db_session.add(empleado)
    db_session.commit()
