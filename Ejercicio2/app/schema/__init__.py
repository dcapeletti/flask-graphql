import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from app.models import db_session, Department as DepartmentModel, Employee as EmployeeModel
from app.schema.employee import *
from app.schema.department import *


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    all_employees = SQLAlchemyConnectionField(Employee)
    # Disable sorting over this field
    all_departments = SQLAlchemyConnectionField(Department, sort=None)
    find_employee_by_name = graphene.Field(
        lambda: Employee, name=graphene.String())
    find_department_by_name = graphene.Field(
        lambda: Department, name=graphene.String())

    def resolve_find_employee_by_name(self, info, **kwargs):
        query = Employee.get_query(info)
        name = kwargs['name']
        return query.filter(EmployeeModel.name == name).first()

    def resolve_find_department_by_name(self, info, **kwargs):
        query = Department.get_query(info)
        name = kwargs['name']
        return query.filter(DepartmentModel.name == name).first()


class MyMutations(graphene.ObjectType):
    # create_employee = CreateEmployee.Field()
    edit_employee = EditEmployee.Field()
    delete_employee = DeleteEmployee.Field()
    create_department = CreateDepartment.Field()
    edit_department = EditDepartment.Field()
    delete_department = DeleteDepartment.Field()


schema = graphene.Schema(query=Query, mutation=MyMutations, types=[Employee, Department])
