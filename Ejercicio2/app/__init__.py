from flask import Flask
from flask_graphql import GraphQLView

from app.models import db_session
from app.schema import schema

app = Flask(__name__)
app.debug = True

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True # for having the GraphiQL interface
    )
)

@app.route('/')
def goto():
    return "Ir a /graphql"

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

from app.schema.employee.employee_rest import *
# if __name__ == '__main__':
#     app.run()
