import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from database import db_session,User as UserModel
# from sqlalchemy import and_

class Users(SQLAlchemyObjectType):
	class Meta:
	    model = UserModel
	    interfaces = (relay.Node, )

# Used to Create New User
'''
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:5000/graphql?' --data '{
"query": "mutation { createUser(name: \"Jose cablar\", email: \"jose cablar@dsda.com\", username: \"jcablar\") { user { id name email username } ok } }"
}'
'''
class createUser(graphene.Mutation):
	class Input:
		name = graphene.String()
		email = graphene.String()
		username = graphene.String()
	ok = graphene.Boolean()
	user = graphene.Field(Users)

	@classmethod
	def mutate(cls, _, args, context, info):
		user = UserModel(name=args.get('name'), email=args.get('email'), username=args.get('username'))
		db_session.add(user)
		db_session.commit()
		ok = True
		return createUser(user=user, ok=ok)


# Used to Change Username with Email
'''
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:5000/graphql?' --data '{
"query": "mutation { changeUsername(email: \"hello@abc.com\", username:\"dcapeletti1\") { user { name, email, username } } }"
}'
'''
class changeUsername(graphene.Mutation):
	class Input:
		username = graphene.String()
		email = graphene.String()

	ok = graphene.Boolean()
	user = graphene.Field(Users)

	@classmethod
	def mutate(cls, _, args, context, info):
		query = Users.get_query(context)
		email = args.get('email')
		username = args.get('username')
		user = query.filter(UserModel.email == email).first()
		user.username = username
		db_session.commit()
		ok = True

		return changeUsername(user=user, ok = ok)

'''
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:5000/graphql?' --data '{
"query": "mutation { removeUser(id: 2){ user {id, name, username} ok } }"
}'
'''
class DeleteUser(graphene.Mutation):
	class Input:
		id = graphene.Int()

	ok = graphene.Boolean()
	user = graphene.Field(Users)

	@classmethod
	def mutate(cls, _, args, context, info):
		query = Users.get_query(context)
		id = args.get('id')
		user = query.filter(UserModel.id == id).first()
		db_session.delete(user)
		db_session.commit()
		ok = True
		return DeleteUser(user=user, ok = ok)



class Query(graphene.ObjectType):
	node = relay.Node.Field()
	user = SQLAlchemyConnectionField(Users)
	find_user_by_id = graphene.Field(lambda: Users, id = graphene.Int())
	find_user_by_username = graphene.Field(lambda: Users, username = graphene.String())
	all_users = SQLAlchemyConnectionField(Users)

	def resolve_find_user_by_username(self,args,context,info):
		'''
		curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:5000/graphql?' --data '{
		"query": "{ findUserByUsername(username: \"jcablar\") { id, name, email } }"
		}'
		'''
		query = Users.get_query(context)
		print(args)
		username = args.get('username')
		# you can also use and_ with filter() eg: filter(and_(param1, param2)).first()
		return query.filter(UserModel.username == username).first()

	def resolve_find_user_by_id(self,args,context,info):
		'''
		curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:5000/graphql?' --data '{
		"query": "{ findUserById(id: 3){ name, username } }"
		}'
		'''
		query = Users.get_query(context)
		print(args)
		id = args.get('id')
		# you can also use and_ with filter() eg: filter(and_(param1, param2)).first()
		return query.filter(UserModel.id == id).first()


class MyMutations(graphene.ObjectType):
	create_user = createUser.Field()
	change_username = changeUsername.Field()
	remove_user = DeleteUser.Field()

schema = graphene.Schema(query=Query, mutation=MyMutations, types=[Users])
